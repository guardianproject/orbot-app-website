## Orbot webpage - Jekyll version

### ☁️ Installation & Local Usage:

You need Ruby and gem before starting.
When Ruby & Jekyll installed:

```bash
# clone the project
> clone this repo

# navigate to the folder
> cd orbot-app-website

# install project dependencies
> bundle install

# run jekyll with dependencies
> bundle exec jekyll serve

# build the project
> bundle exec jekyll build
```

### Adding new languages

Add the BCP47 language code to the `locales` array on `_config.yml`:

```yaml
locales:
- "en"
- "en_US"
```

**Note:** The first locale in the list is considered the main language.

If you add a specific variant (ie. "pt-BR" or "zh-Hans") but don't
support the main variant ("pt", "zh"), users using these languages will
be redirected to the default language ("en") instead of being sent to
a probably wrong option.
