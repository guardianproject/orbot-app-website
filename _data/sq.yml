dir: "mnd"
site:
  title: 'Orbot - Tor për Celularin'
bottom_contact_area:
  title: "Lidhuni."
  content: "Doni ta përkrahni financiarisht në rrugë tjetër projektin? Ju lutemi,
    lidhuni me ne."
  href: "https://guardianproject.info/contact"
  action: "Kontakt"
bottom_donate_area:
  title: "I lirë dhe i hapët."
  content: "Orbot-i është aplikacion i lirë! Përkrahni zhvillimin e vazhdueshëm të
    aplikacionit. Përhapni dashuri."
  action: "Dhuroni"
footer:
  copyright: "Të drejta Kopjimi © 2009-%Y Guardian Project. Krejt të drejtat të rezervuara."
main_navigation:
  donate: "Dhuroni"
  responsive_menu_links:
  - url: "/"
    title: "Kreu"
  - url: "/about/"
    title: "Rreth"
  - url: "/privacy-policy/"
    title: "Privatësi"
  - url: "/legal/"
    title: "Ligjore"
  - url: "/download/"
    title: "Shkarkim"
  - url: "/faqs/"
    title: "PBR"
  - url: "/donate/"
    title: "Dhuroni"
locale_selector:
  title: "Gjuhë"
about:
  special_area:
    title: 'Orbot - VPN Tor-i për Telefona të Mençur'
    subtitle: 'Mbajeni privat dhe të zhbllokuar trafikun e aplikacioneve tuaja.'
  title: 'Orbot - Mbajini të Parrezik Aplikacionet'
  description: 'Orbot-i është një VPN Tor-i për Android. I mban të parrezik aplikacionet!'
  tor_area:
    image: '/assets/img/Tor.png'
    description: 'Tor-i mbahet në këmbë nga një rrjet i larmishëm personash. Kur përdorni
      Tor-in, asnjë shoqëri apo person s’ka hyrje te veprimtaria juaj e shfletimit.'
    text: 'Mësoni rreth Tor-it te'
    url: 'https://torproject.org'
    tor_items:
    - image: '/assets/img/EncryptedTraffic.png'
      alt: 'EncryptedTraffic-icon'
      title: 'Trafik i fshehtëzuar.'
      text: 'Trafiku juaj internet fshehtëzohet, mandej kalohet nëpër tre pjesë të
        ndryshme të botës, para se të mbërrijë atje ku e nisët.'
    - image: '/assets/img/OnlinePrivacy.png'
      alt: 'OnlinePrivacy-icon'
      title: 'Privatësi Internetore.'
      text: 'Palët e treta s’do ta dinë se ç’aplikacione po përdorni. Adresa juaj
        e njëmendtë IP nuk nxirret sheshit.'
    - image: '/assets/img/NoCensorship.png'
      alt: 'NoCensorship-icon'
      title: 'Pa censurë.'
      text: 'Fitoni hyrje te aplikacione dhe lëndë, pavarësisht kufizimeve tuaja gjeografike
        apo të rrjetit.'
    - image: '/assets/img/NoSurveillance.png'
      alt: 'NoSurveillance-icon'
      title: 'Pa survejim.'
      text: 'Funizuesit e shërbimeve Internet (ISP) dhe operatorët e rrjeteve celulare
        s’kanë për ta parë shfletimin tuaj.'
    - image: '/assets/img/NoTracking.png'
      alt: 'NoTracking-icon'
      title: 'Pa gjurmim.'
      text: 'Kontrollin e keni ju. Çaktivizoni ndjekjen tuaj nga palë të treta.'
  connected_area:
    image: '/assets/img/AboutScreens.png'
    link: 'https://www.csoonline.com/article/3287653/what-is-the-tor-browser-how-it-works-and-how-it-can-help-you-protect-your-identity-online.html'
  disclaimer_area:
    disclaimer_items:
    - title: 'Klauzolë'
      text_nodes:
      - node: 'Përdorimi i Orbot-it nuk garanton siguri ose privatësi në vetvete.
          Thjesht jep një grup veçorish që mund të zgjerojnë privatësinë dhe anonimitetin
          e trafikut tuaj në rrjet. Ju lutemi, mbani mend sa vijon…'
      - node: 'Nëse përdorni Orbot-in për të bërë hyrje në aplikacione që normalisht
          i përdorni jashtë rrjetit Tor, aplikacioni mund të jetë në gjendje t’ju
          identifikojë dhe të dijë që po përdorni Tor-in. Në disa rrethana (p.sh.,
          disidentë politikë në vende represive), kjo mund të përbëjë në vetvete informacion
          komprometues.'
    - title: 'Gjeolokalizim, Media dhe Informacion Celulari'
      text_nodes:
      - node: 'Aplikacionet mund të përdorin API Gjeolokalizimi (për të parë vendndodhjen
          tuaj aktuale GPS), të hyjnë në foto dhe video në depozitë të jashtme, ose
          të përftojnë të dhëna rreth telefonit tuaj celular, b.f., numër telefoni
          dhe hollësi shërbimi celular. Përdoruesit duhet të mbesin gjithashtu vigjilentë
          për çfarëdo flluskash që kërkojnë leje për hyrje/përdorim.'
    - title: 'Më Tepër Informacion'
      text_nodes:
      - node: 'Projekti Tor <a href="https://support.torproject.org/faq/staying-anonymous/">mirëmban
          një faqe të vogël ndihmëzash</a> rreth qëndrimit anonim, teksa përdoret
          Tor-i.'
      - node: 'Një regjistër i <a href="https://github.com/guardianproject/orbot/releases">hedhjeve
          në qarkullim</a> të Orbot-it, përfshi probleme të dikurshëm sigurie, ndreqje
          të metash, etj <a href="https://github.com/guardianproject/orbot/releases">mund
          të kihet këtu</a>. Mund të shihni edhe projektin GitHub, për një <a href="https://github.com/guardianproject/orbot/issues">listë
          të tanishme të problemeve të ditura.</a>'
code:
  title: 'Kod'
  description: 'Kod me Burim të Hapët i Orbot-it'

  title_section: 'Kod'
  code_items:
  - question: 'Orbot për Android'
    answer: 'Kodi kryesor mund të kihet që nga [GitHub](https://github.com/guardianproject/orbot/)
      and [Gitlab](https://gitlab.com/guardianproject/orbot/)'
  - question: 'Orbot për iOS'
    answer: 'Kodi kryesor mund të kihet që nga [GitHub](https://github.com/guardianproject/orbot-ios)'
  - question: 'Tor-Android'
    answer: 'Orbot-i montohet me bibliotekën [Tor-Android](https://gitlab.com/guardianproject/tor-android/)
      si bazë, çka i lejon cilitdo aplikacion ta paketojë dhe administrojë Tor-in
      brenda vetes'
  - question: 'Tor.framework për iOS'
    answer: 'Orbot-i për iOS montohet me projektin [Tor.framework](https://github.com/iCepa/Tor.framework/tree/pure_pod)
      si bazë, i cili mund të kihet si CocoaPad për t’u integruar nga çfarëdo aplikacioni'
  - question: 'IPtProxy'
    answer: 'Duke përdorur [IPtProxy Library](https://github.com/tladesignz/IPtProxy),
      Orbot-i përfshin gjithashtu mbulim për Transporte të Heqshëm Tor, b.f., Obfs4
      dhe [Snowflake](https://snowflake.torproject.org/)'
donate:
  title: 'Përkrahni Orbot-in'
  description: 'Orbot-i zhvillohet nga Guardian Project, një ent i financuar thuajse
    tërësisht nga grante dhe dhurime prej entesh dhe individësh.'
  subtitle: 'Rrugë për të dhuruar'
  donate_form:
  - text: 'Dërgoni $10.00 USD'
    value: '10'
  - text: 'Dërgoni $20.00 USD'
    value: '20'
  - text: 'Dërgoni $30.00 USD'
    value: '30'
  - text: 'Dërgoni $40.00 USD'
    value: '40'
  - text: 'Dërgoni $50.00 USD'
    value: '50'
  donate_partners:
  - name: 'PayPal'
    url: 'https://paypal.me/guardianproject'
    image: '/assets/img/pp.jpg'
    alt: 'PayPal-logo'
    text: 'Dërgoni një kontribut vetëm për një herë te'
  - name: 'BitCoin'
    url: '198fHnKSkXboZLWqVi2KTRdNrPbdh34Ya5'
    image: '/assets/img/Bitcoin.png'
    alt: 'Bitcoin-logo'
  - name: 'Ether'
    url: '133170647542866872072615948397500080356915441689'
    image: '/assets/img/Ether.png'
    alt: 'Ether-logo'
  - name: 'Patreon'
    url: 'https://patreon.com/GuardianProject'
    image: '/assets/img/Patreon.png'
    alt: 'Patreon-logo'
    text: 'Na përkrahni në'
  - name: 'Liberapay'
    url: 'https://liberapay.com/GuardianProject'
    image: '/assets/img/liberapay.svg'
    alt: 'Liberapay-img'
    text: 'Dhuroni për ne te'
  - name: 'Sponsor GitHub'
    url: 'https://github.com/sponsors/eighthave'
    image: '/assets/img/github.png'
    alt: 'github-logo'
    text: 'Na sponsorizoni në'
download:
  special_area:
    title: 'Orbot - VPN Tor-i për Telefona të Mençur'
    subtitle: 'Shkarkoni Orbot-in!'
  tor_area:
    text: 'Shkarkoni <em>software</em> tjetër të Projektit Tor te'
    url: 'https://www.torproject.org/download/'
    tor_items:
    - title: 'Google Play'
      info: 'Aplikacionin mund ta instaloni që nga Google Play këtu te [https://play.google.com/store/apps/details?id=org.torproject.android](https://play.google.com/store/apps/details?id=org.torproject.android)'
    - title: 'F-Droid'
      info: 'Si të shtoni depon F-Droid të aplikacioneve Guardian Project F-Droid
        mund ta mësoni te [https://guardianproject.info/fdroid](https://guardianproject.info/fdroid)'
    - title: 'Apple App Store'
      info: 'Mund të instaloni hedhjen zyrtare në qarkullim të Orbot-it për iOS që
        nga [https://apps.apple.com/us/app/orbot/id1609461599](https://apps.apple.com/us/app/orbot/id1609461599).
        Mundeni gjithashtu të bëheni pjesë e grupit tonë të përshtypjeve të hershme,
        Testflight, përmes [https://testflight.apple.com/join/adSqbCeM](https://testflight.apple.com/join/adSqbCeM)'
    - title: 'Shkarkim i Drejtpërdrejtë'
      info: 'Mund të gjeni hedhje në qarkullim të etiketuara me kartela të shkarkueshme
        për testim në GitHub te [https://github.com/guardianproject/orbot/releases](https://github.com/guardianproject/orbot/releases)
        dhe [https://github.com/guardianproject/orbot-ios/releases](https://github.com/guardianproject/orbot-ios/releases)'
    - title: 'Sajti i Projektit Guardian'
      info: 'Mund të shkarkoni kartelën (APK) të aplikacionit Orbot Android drejt
        e nga [https://guardianproject.info/releases/orbot-latest.apk](https://guardianproject.info/releases/orbot-latest.apk)'
faqs:
  title: 'Orbot - PBR'
  description: 'Faqe PBR e Orbot-it'
  title_section: 'PBR'
  faq_items:
  - question: 'Ku mund të gjej informacion mbi hedhjen më të re në qarkullim, ndreqje
      dhe përmirësime?'
    answer: 'Postojmë krejt hedhjet në qarkullim dhe shënime për to në faqet tona
      të projektit me burim të hapët te [https://github.com/guardianproject/orbot/releases](https://github.com/guardianproject/orbot/releases)
      dhe [https://github.com/guardianproject/orbot-ios/releases](https://github.com/guardianproject/orbot-ios/releases)'
  - question: 'Ç’është Tor-i?'
    answer: 'Tor-i është një rrjet relesh të mbajtura në punë nga vullnetarë anembanë
      botës, që ju lejon t’i kaloni komunikimet tuaja në to, çka ju bën të mundur
      t’u fshini sajtet që vizitoni personave që vëzhgojnë lidhjen tuaj internet,
      u pengon gjithashtu sajteve që vizitoni të njohin vendndodhjen tuaj konkrete.'
  - question: 'Pse është më i ngadaltë interneti, kur jam i lidhur në Tor?'
    answer: 'Ngaqë e kaloni trafikun tuaj nëpër rele të mbajtura në punë nga vullnetarë
      anembanë botës dhe lidhja juaj do të preket nga ngecje dhe vonesa rrjeti.'
  - question: 'Si ta di se jam i lidhur në Tor?'
    answer: 'Pasi të hapni Orbot-in, shtypni butonin e madh dhe pas pak sekondash
      që vendoset lidhje me Tor-in do të shihni një mesazh ku thuhet 100% i lidhur
      dhe butoni do të bëhet i gjelbër. Nëse përdorni një VPN për të kaluar trafikun
      e shfletuesit përmes Tor-i, mund ta kontrolloni lidhjen tuaj duke kaluar te
      [https://check.torproject.org/](https://check.torproject.org/), një lidhje e
      krijuar nga ekipi i Tor-it për t’ju treguar nëse jeni apo jo të lidhur në Tor.'
  - question: 'Ç’janë urat?'
    answer: 'Urat janë rele Tor që ndihmojnë të anashkalohet censurimi. Mund të provoni
      ura, nëse Tor-i është i bllokuar nga ISP-a juaj.'
  - question: Pse u bë kaq i paqëndrueshëm Orbot-i për iOS?
    answer: Sulmet mbi rrjetin Tor u shtuan, që prej luftës në Ukrainë. Prej atëherë,
      janë ndrequr cenueshmëri sigurie dhe janë shtuar dhe nyja, por, nga ana tjetër,
      kjo ha më tepër kujtesë. Kienti Tor është i ndjeshëm ndaj ndryshimesh në rrjet,
      ngaqë dëshiron të zbulojë gjithçka për të. Sa më tepër nyja gjen, aq më tepër
      kujtesë përdor. Mjerisht, Apple-i lejon përdorimin e vetëm 50 MB RAM (megabajte,
      në një pajisje që ka të paktën 3 GB - GIGAbajte!) në të ashtuquajturat “Zgjerime
      Rrjeti” (API që duhet përdorur për aplikacione në stil “VPN”-je). Ky është kufi
      shumë i pafavorshëm për <em>software</em> të tillë si Tor-i. Veç kësaj, Tor-i
      origjinal i përdorur tani, i shkruar në C është duke u nxjerrë në pension, ndërkohë
      që një sendërtim i ri i Tor-it i shkruar në Rust është duke ardhur, por jo atje
      ku duhet të jetë, ende jo. Ju lutemi, bëni durim.
  - question: Pse lidhet paprerë Orbot-i për iOS?
    answer: Shihni përgjigjen më sipër – për shkak të madhësisë së pjesës së rrjetit
      Tor që shihni, mund të mbërrini në kufirin 50 MB. iOS e asgjëson “Zgjerimin
      e Rrjetit”, kur është kështu. Nëse keni përzgjedhur “rinisu, kur ka gabim” (aktive,
      si parazgjedhje), do të provohet vetvetiu të riniset.
  - answer: Provoni të fshini fshehtinën. Ndonjëherë, heqja qafe e informacioneve
      të vjetra mund të çlirojë sasi të mjaftueshme kujtese. Por, zbulimi nga e para
      e çdo nyje faktikisht përdor *më tepër* kujtesë se sa ngarkimi i tyre prej fshehtine.
      Ndaj, nëse s’niset herën e parë, jepini më tepër shanse për t’u rinisur. Do
      të ngarkojë gjithnjë e më tepër informacion të tanishëm, çka lë më tepër kujtesë
      për veprimet e zakonshme.
    question: Si mund ta bëj Orbot-in për iOS që të funksionojë?
  - question: Niset, me një fshehtinë të freskët, por pas pak vithiset sërish.
    answer: Kaloni te Rregullime, aktivizoni “Fshi përherë fshehtinën, para nisjes”.
      Do të zgjasë më shumë, por do të riniset, për sa kohë që pjesa e rrjetit që
      shihni, s’është shumë e madhe për një nisje të freskët.
  - answer: E vëmë të ulët këtë (5 MB, si parazgjedhje), që të mos mbërrijmë shumë
      shpejt në tavanin 50 MB. Mund të eksperimentoni duke e rritur. Kaloni te Rregullime.
      Jepni `--MaxMemInQueues` (dy minuse!) në një rresht te “Formësim i Thelluar
      Tor-i”, jepni `10 MB` në rreshtin pasues. Riniseni. Nëse përfundoni në një qerthull
      rinisjesh, po përdorni shumë kujtesë. Në një rast të tillë, hiqni prapë këto
      rreshta.
    question: Tor-i ankohet te regjistri se “MaxMemInQueues” është shumë e ulët dhe
      se s’mund të ndërtojë qarqe!
  - question: Asgjë s’bën punë, Tor-i nuk niset.
    answer: Provoni të përdorni ura të përshtatura, edhe pse nuk ju duhen për të anashkaluar
      bllokimin. Pjesa e rrjetit Tor që shihni përmes urash mund të jetë më e vogël,
      kështu që klienti Tor s’do të konsumojë shumë kujtesë.
  - question: Urat duket se i bllokojnë!
    answer: Klikoni “Kërkojini Tor-it” – do të përditësojë listën e urave të brendshme
      Obfs4, do të përditësojë formësimin për Snowflake-un dhe do t’ju japë një dorë
      urash të përshtatura. Provoni sërish krejt kombinimet. Mundeni edhe të përdorni
      Telegram-in, ose robotin email, ata do të japin ura të ndryshme, nga grupe të
      tjera.
  - answer: Apple pruri një vizatues të ri (më të shpejtë) web, të quajtur `WKWebView`,
      i cili zëvendësoi `UIWebView` dhe donte që krejt aplikacionet të kalonin nën
      të. Porr, `WKWebView` nuk mbulonte kalimin e trafikut nëpër ndërmjetës në mënyrën
      që bënte `UIWebView`. Përsipër këtyre, kjo qe përherë një lloj paterice, ngaqë
      s’kalonim dot kurrë rrjedha audio/video përmes ndërmjetësish dhe nxirrnim zbuluar
      adresën tuaj IP, përmes WebRTC-së. Me Orbot-in, këto probleme hiqen qafe. Mjerisht,
      Orbot iOS tani aksidentalisht përfundoi në këtë qoshe, prej nga është e vështirë
      të dilet. Që me iOS 17, `WKWebView` mbulon përdorim ndërmjetësish, ndaj tani
      prapë keni një zgjedhje. Ju lutemi, përditësoje me iOS 17, nëse mundeni!
    question: Pse bazohet në Orbot për iOS versioni i ri i Shfletuesit Onion?
home:
  title: 'Orbot - Mbajini të Parrezik Aplikacionet.'
  description: 'Orbot-i është një VPN Tor për Android. I mban të parrezik aplikacionet!'
  locale: 'Anglisht'
  banner_area:
    subtitle: 'Orbot - VPN Tor-i për Telefona të Mençur'
    title: 'Mbajini të Parrezik Aplikacionet'
    app_store_url: 'https://play.google.com/store/apps/details?id=org.torproject.android'
    app_store_button: '/assets/img/en-play-badge.png'
    apple_url: 'https://apps.apple.com/us/app/orbot/id1609461599'
    apple_button: '/assets/img/apple.png'
    fdroid_url: 'https://guardianproject.info/fdroid'
    fdroid_button: '/assets/img/fdroid.png'
    image: '/assets/img/LandingPage.png'
  promo_area:
    promo_area_items:
    - title: 'Privatësi Trafiku'
      text: 'Trafiku i fshehtëzuar, prej cilitdo aplikacion, përmes rrjetit Tor, ju
        jep standardin më të lartë të sigurisë dhe privatësisë.'
      image: '/assets/img/Browsing.png'
      alt: 'Browsing-icon'
    - title: 'Ndalni Përgjimin'
      text: 'Se ç’aplikacione përdorni dhe kur, apo nëse reshtni së përdorni ato,
        s’e dinë sy të tjerë.'
      image: '/assets/img/NoExtras.png'
      alt: 'NoExtras-icon'
    - title: 'Historik i Mbrojtur'
      text: 'Pa regjistrim të qendërzuar të historikut të trafikut tuaj, apo adresës
        tuaj IP nga operatori i rrjetit tuaj dhe shërbyesit e aplikacioneve.'
      image: '/assets/img/TossHistory.png'
      alt: 'TossHistory-icon'
  power_area:
    title: 'Bazuar në Tor'
    text: 'Orbot-i është lidhja juaj e besuar për Tor në Android dhe iOS.'
    tor_image: '/assets/img/Tor.png'
    link_text: 'Mësoni më tepër >'
    link_url: 'about'
  detail_area:
    detail_area_items:
    - title: 'Mbroni dhe zhbllokoni<br/>aplikacione specifike.'
      text: 'Orbot-i (në Android) ju lejon të zgjidhni në mënyrë specifike cilët aplikacione
        të kalohen përmes Tor-i, duke ju lejuar të përdorni ende aplikacione dhe shërbime
        që mund ta kenë problem trafikun e ardhur prej Tor-i.'
      image: '/assets/img/AppChoice.jpg'
      alt: 'AppChoice-image'
    - title: 'Hyrje super e shpejtë, super e siguruar në shërbime të njohur Onion.'
      text: '<a href="https://support.torproject.org/onionservices/" target="_blank">Shërbimet
        Onion</a> janë versione sajtesh që mund të përdoret vetëm përmes Tor-i. Orbot-i
        i lejon cilitdo aplikacion të lidhet me një shërbim Onion dhe telefonit tuaj
        të strehojë vetë ai një shërbim Onion!'
      image: '/assets/img/OnionSites.jpg'
      alt: 'OnionSites-mage'
    - title: 'Jini i Lidhur'
      text: 'Orbot-i ofron hyrje te një larmi Urash Tor, që të mund të mbesni të lidhur
        edhe në rrjetet me më tepër kufizime'
      image: '/assets/img/Bridges.jpg'
      alt: 'Bridges-image'
  featured_area:
    title: 'Paraqitur në'
    featured_area_items:
    - image: '/assets/img/NewYorkTimes.png'
      alt: 'NewYorkTimes-logo'
      url: "http://www.nytimes.com/2014/02/20/technology/personaltech/privacy-please-tools-to-shield-your-smartphone-from-snoopers.html"
    - image: '/assets/img/ArsTechnica.jpg'
      alt: 'ArsTechnica-logo'
      url: "https://arstechnica.com/information-technology/2012/02/from-encryption-to-darknets-as-governments-snoop-activists-fight-back/"
    - image: '/assets/img/TechCrunch.png'
      alt: 'TechCrunch-logo'
      url: "https://techcrunch.com/2016/01/20/facebook-expands-tor-support-to-android-orbot-proxy/"
    - image: '/assets/img/BoingBoing.png'
      alt: 'BoingBoing-logo'
      url: "https://boingboing.net/2018/10/23/hardware-orbot.html"
    - image: '/assets/img/LifeHacker.png'
      alt: 'LifeHacker-logo'
      url: "https://lifehacker.com/the-apps-that-protect-you-against-verizons-mobile-track-1679936224"
    - image: '/assets/img/Gizmodo.png'
      alt: 'Gizmodo-logo'
      url: "https://gizmodo.com/6-apps-to-secure-your-smartphone-better-1791777911"
  special_area:
    title: '“Orbot-i është rruga më e parrezik për të qenë i lidhur me aplikacionet
      që më duhen!”'
    text: 'Shqyrtim në Play Store - 21 maj 2018'
  dev_area:
    title: 'Rreth zhvilluesve.'
    text: 'Orbot-i është <a href="https://github.com/guardianproject/orbot/">i lirë
      dhe me burim të hapët</a>. Cilido mund të ndihmojë duke kontribuar, peshuar
      apo inspektuar kodin. Në programuesit me kontribut kryesor përfshihen <a href="https://github.com/n8fr8">Nathan
      Freitas</a>, <a href="https://gitlab.com/eighthave">Hans-Christoph Steiner</a>,
      <a href="https://github.com/bitmold">Bim</a>, <a href="https://die.netzarchitekten.com/">Benjamin
      Erhart</a>, etj prej <a href="https://guardianproject.info/">Guardian Project</a>.'
    github_url: 'https://github.com/guardianproject/orbot'
    github_label: 'Ndiqeni @GuardianProject në GitHub'
    github_text: 'Ndiqni @GuardianProject'
legal:
  title: 'Orbot | Ligjore'
  description: 'Orbot - Faqja ligjore.'
  title_section: 'Ligjore'
  legal_items:
  - text: |
      **Orbot** është e drejtë kopjimi e © 2009-2022 Nathan Freitas dhe Guardian Project. Krejt të drejtat të rezervuara. **"Orbot"** dhe stema e Orbot-it janë shenja tregtare të Nathan Freitas. Orbot-i jepet sipas termave të licencës [3-clause BSD license](https://opensource.org/licenses/BSD-3-Clause)
  - text: 'Ky program përdor kriptografi të fuqishme dhe mund të jetë objekt i disa
      kufizimesh eksportimi/importimi ose përdorimi, në disa pjesë të botës. PARA
      përdorimit të çfarëdo software-i fshehtëzimesh, ju lutemi, shihni ligjet e vendit
      tuaj, rregulloret dhe rregulla që lidhen me importim, pasje në zotërim, ose
      përdorim dhe rieksportim software-i fshehtëzimi, për të parë nëse kjo lejohet.
      Për më tepër informacion, shihni [https://www.wassenaar.org](https://www.wassenaar.org).'
  - text: 'Nëse jeni një zhvillues dhe keni në plan të rishpërndani këtë aplikacion
      në formën burim, ose dyore, ju lutemi, lexoni [kartelën e LICENCËS](https://github.com/guardianproject/orbot/blob/master/LICENSE)
      për hollësi të plota licence dhe detyrimesh ligjore.'
navigation:
  navigation_links:
  - title: 'Rreth'
    url: '/about'
  - title: 'Privatësi'
    url: '/privacy-policy'
  - title: 'Shkarkim'
    url: '/download'
  - title: 'Kod'
    url: '/code'
  - title: 'PBR'
    url: '/faqs'
  - title: 'Ligjore'
    url: '/ligjore'
  - title: 'Dhuroni'
    url: '/dhuroni'
privacy:
  title: 'Orbot | Rregulla Privatësie'
  description: 'Orbot - faqe Rregullash Privatësie.'
  title_section: 'Rregulla Privatësie'
  privacy_items:
  - text: "Orbot-i nuk grumbullon drejtpërsëdrejti ndonjë të dhënë mbi veprimtarinë
      e përdoruesit. Hiq ç’është ndërtuar brenda dhe ndiqet nga Google Play, Orbot-i
      s’përdor statistika nga ndonjë palë e tretë."
  - text: "Më tepër rreth se si mbron Tor-i privatësinë tuaj mund të mësoni duke lexuar
      [PBR për Tor-in](https://2019.www.torproject.org/docs/faq.html.en)"
  - text: "Më tepër rreth trajtimit që Guardian Project i bën privatësisë mund të
      mësoni përmes faqes [Përdorim të Dhënash dhe Rregulla Mbrojtjeje](https://guardianproject.info/2016/05/04/data-usage-and-protection-policies/)
      page."
