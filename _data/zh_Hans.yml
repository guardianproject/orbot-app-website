site:
  title: 'Orbot - Tor 移动版'
bottom_contact_area:
  title: "联系我们。"
  content: "想要以其他方式为该项目提供经济支持吗？ 请保持联系。"
  href: "https://guardianproject.info/contact"
  action: "联系方式"
bottom_donate_area:
  title: "自由和开放。"
  content: "Orbot 是自由应用！请支持应用的持续开发。分享爱。"
  action: "捐款"
footer:
  copyright: "Copyright © 2009-%Y Guardian 项目。版权所有。"
main_navigation:
  donate: "捐款"
  responsive_menu_links:
  - url: "/"
    title: "主页"
  - url: "/about/"
    title: "关于"
  - url: "/privacy-policy/"
    title: "隐私"
  - url: "/legal/"
    title: "法律"
  - url: "/download/"
    title: "下载"
  - url: "/faqs/"
    title: "常见问题"
  - url: "/donate/"
    title: "捐赠"
locale_selector:
  title: "语言"
about:
  special_area:
    title: 'Orbot - 适用于智能手机的 Tor VPN'
    subtitle: '让您的应用流量保持私密，不受屏蔽。'
  title: 'Orbot - 让应用更安全'
  description: 'Orbot 是适用于 Android 的 Tor VPN。 它可以保证应用程序的安全！'
  tor_area:
    image: '/assets/img/Tor.png'
    description: 'Tor 由多元化的人际网络提供支持。当您使用 Tor 时，任何公司或个人都无法访问您的浏览活动。'
    text: '了解 Tor 的相关信息'
    url: 'https://torproject.org'
    tor_items:
    - image: '/assets/img/EncryptedTraffic.png'
      alt: 'EncryptedTraffic-icon'
      title: '加密流量。'
      text: '您的网络流量经过加密，然后通过全球三个不同的地方跳转，最后到达目的地。'
    - image: '/assets/img/OnlinePrivacy.png'
      alt: 'OnlinePrivacy-icon'
      title: '在线隐私。'
      text: '第三方不会知道您正在使用哪些应用，您的真实 IP 地址不会泄露。'
    - image: '/assets/img/NoCensorship.png'
      alt: 'NoCensorship-icon'
      title: '没有审查制度。'
      text: '无论您的地理位置或网络限制如何，都可以访问应用和内容。'
    - image: '/assets/img/NoSurveillance.png'
      alt: 'NoSurveillance-icon'
      title: '没有监视。'
      text: '互联网服务提供商（ISP）和 WiFi 网络运营商无法看到您的浏览内容。'
    - image: '/assets/img/NoTracking.png'
      alt: 'NoTracking-icon'
      title: '没有跟踪。'
      text: '由您掌控一切。禁止第三方跟踪您。'
  connected_area:
    image: '/assets/img/AboutScreens.png'
    link: 'https://www.csoonline.com/article/3287653/what-is-the-tor-browser-how-it-works-and-how-it-can-help-you-protect-your-identity-online.html'
  disclaimer_area:
    disclaimer_items:
    - title: '免责声明'
      text_nodes:
      - node: '使用 Orbot 本身并不能保证安全或隐私。它只是提供了一套可以增强网络流量的隐私性和匿名性的功能。请记住以下几点……'
      - node: '如果您使用 Orbot 登录您通常在 Tor 网络之外访问的应用，该应用可能能够识别您的身份并知道您正在使用 Tor。在某些情况下（例如专制国家的政治异议），这本身可能就是有罪的信息。'
    - title: '地理位置、媒体和移动信息'
      text_nodes:
      - node: '应用可能会使用地理定位 API（查看您当前的 GPS 位置）、访问外部存储中的照片和视频，或提取有关您手机的数据，例如电话号码和运营商信息。用户还应该对任何请求访问权限的弹出窗口保持警惕。'
    - title: '更多信息'
      text_nodes:
      - node: 'Tor 项目<a href="https://support.torproject.org/faq/staying-anonymous/">维护了一小页</a>关于在使用
          Tor 时保持匿名的提示。'
      - node: 'Orbot 发布说明的<a href="https://github.com/guardianproject/orbot/releases">日志</a>，包括以前的安全问题、错误修复等，都可以在<a
          href="https://github.com/guardianproject/orbot/releases">此处</a>找到。您还可以在
          GitHub 项目中查看<a href="https://github.com/guardianproject/orbot/issues">当前已知问题的列表</a>。'
code:
  title: 'Orbot | 代码'
  description: 'Orbot 开源代码'

  title_section: '代码'
  code_items:
  - question: 'Android 版 Orbot'
    answer: '主要代码可在 [GitHub](https://github.com/guardianproject/orbot/) 和 [Gitlab](https://gitlab.com/guardianproject/orbot/)
      上找到'
  - question: 'iOS 版 Orbot'
    answer: '主要代码可在 [GitHub](https://github.com/guardianproject/orbot-ios) 上找到'
  - question: 'Tor-Android'
    answer: 'Orbot 构建在核心库 [Tor-Android](https://gitlab.com/guardianproject/tor-android/)
      之上，该库允许任何应用程序在其中捆绑和管理 Tor'
  - question: '适用于 iOS 的 Tor.framework'
    answer: 'iOS 版 Orbot 构建于核心库[Tor.framework](https://github.com/iCepa/Tor.framework/tree/pure_pod)
      项目，该项目可作为 CocoaPad 供任何应用程序集成'
  - question: 'IPtProxy'
    answer: 'Orbot 还支持 Tor 可插拔传输，例如使用 [IPtProxy 库](https://github.com/tladesignz/IPtProxy)
      的 Obfs4 和 [Snowflake](https://snowflake.torproject.org/)'
donate:
  title: '支持 Orbot'
  description: 'Orbot 由 Guardian Project 开发，该组织的资金几乎全部来自组织和个人的赠款和捐赠。'
  subtitle: '捐赠方式'
  donate_form:
  - text: '发送 10.00 美元'
    value: '10'
  - text: '发送 20.00 美元'
    value: '20'
  - text: '发送 30.00 美元'
    value: '30'
  - text: '发送 40.00 美元'
    value: '40'
  - text: '发送 50.00 美元'
    value: '50'
  donate_partners:
  - name: 'PayPal'
    url: 'https://paypal.me/guardianproject'
    image: '/assets/img/pp.jpg'
    alt: 'PayPal-logo'
    text: '发送一次性捐款至'
  - name: 'BitCoin'
    url: '198fHnKSkXboZLWqVi2KTRdNrPbdh34Ya5'
    image: '/assets/img/Bitcoin.png'
    alt: 'Bitcoin-logo'
  - name: 'Ether'
    url: '133170647542866872072615948397500080356915441689'
    image: '/assets/img/Ether.png'
    alt: 'Ether-logo'
  - name: 'Patreon'
    url: 'https://patreon.com/GuardianProject'
    image: '/assets/img/Patreon.png'
    alt: 'Patreon-logo'
    text: '支持我们'
  - name: 'Liberapay'
    url: 'https://liberapay.com/GuardianProject'
    image: '/assets/img/liberapay.svg'
    alt: 'Liberapay-img'
    text: '捐赠给我们'
  - name: 'GitHub 赞助商'
    url: 'https://github.com/sponsors/eighthave'
    image: '/assets/img/github.png'
    alt: 'github-logo'
    text: '赞助我们'
download:
  special_area:
    title: 'Orbot - 适用于智能手机的 Tor VPN'
    subtitle: '下载 Orbot！'
  tor_area:
    text: '下载其他 Tor 项目软件'
    url: 'https://www.torproject.org/download/'
    tor_items:
    - title: 'Google Play'
      info: '您可以从 Google Play 安装本应用，地址：[https://play.google.com/store/apps/details?id=org.torproject.android](https://play.google.com/store/apps/details?id=org.torproject.android)'
    - title: 'F-Droid'
      info: '您可以在 [https://guardianproject.info/fdroid](https://guardianproject.info/fdroid)
        了解如何添加 Guardian 项目到 F-Droid 应用存储库'
    - title: '苹果 App Store'
      info: '您可以在 [https://apps.apple.com/us/app/orbot/id1609461599](https://apps.apple.com/us/app/orbot/id1609461599)
        安装适用于 iOS 的官方版 Orbot。您也可以通过 [https://testflight.apple.com/join/adSqbCeM](https://testflight.apple.com/join/adSqbCeM)
        加入我们的 Testflight 前期反馈小组'
    - title: '直接下载'
      info: '您可以在 GitHub 上的 [https://github.com/guardianproject/orbot/releases](https://github.com/guardianproject/orbot/releases)
        和 [https://github.com/guardianproject/orbot-ios/releases](https://github.com/guardianproject/orbot-ios/releases)
        上找到标记为可下载文件进行测试的版本'
    - title: 'Guardian 项目网站'
      info: '您可以直接在 [https://guardianproject.info/releases/orbot-latest.apk](https://guardianproject.info/releases/orbot-latest.apk)
        下载 Orbot Android 应用程序文件（APK）'
faqs:
  title: 'Orbot - 常见问题解答'
  description: 'Orbot 常见问题解答页面'
  title_section: '常见问题解答'
  faq_items:
  - question: '在哪里可以找到有关最新版本、修复和改进的信息？'
    answer: '我们在开源项目页面上发布了所有版本和发行说明 [https://github.com/guardianproject/orbot/releases](https://github.com/guardianproject/orbot/releases)
      and [https://github.com/guardianproject/orbot-ios/releases](https://github.com/guardianproject/orbot-ios/releases)'
  - question: '什么是 Tor？'
    answer: 'Tor 是由世界各地的志愿者运行的中继网络，可以在其中跳转您的通信，这样就可以隐藏您访问的网站，使其不被监视您网络连接的人发现，还可以防止您访问的网站了解您的物理位置。'
  - question: '为什么我连接到 Tor 后网速会变慢？'
    answer: '因为您的流量会通过世界各地志愿者运行的中继来跳转，并且您的连接将受到阻碍和网络延迟的影响。'
  - question: '我如何知道我已经连接到了 Tor？'
    answer: '打开Orbot后，点击按钮，然后几秒钟后与Tor建立连接，你将看到一个显示“100%已连接”的消息，按钮会变成绿色。如果你正在使用VPN将浏览器的流量通过Tor进行路由，你也可以通过[https://check.torproject.org/](https://check.torproject.org/)检查连接状态，这是一个由Tor团队创建的用于检查您是否连接到Tor的网站。'
  - question: '什么是网桥？'
    answer: '网桥是有助于规避审查的 Tor 中继。如果 Tor 被您的ISP阻止访问，您可以尝试网桥。'
  - question: 为什么Orbot在iOS上变的不可靠？
    answer: 自俄乌战争以来，对 Tor 网络的攻击有所增加。从那时起，安全漏洞得到了修复，并添加了节点，但这再次消耗了更多内存。 Tor 客户端对网络变化很敏感，因为它想要发现所有网络变化。它找到的节点越多，消耗的内存就越多。不幸的是，Apple
      只允许在所谓的“网络扩展”（必须用于“VPN”式应用程序的 API）中使用 50 MB RAM（兆字节，在至少有 3 GB - GIGA 字节的设备上！
      ）。对于 Tor 这样的软件来说，这是一个非常严格的限制。此外，当前使用的用 C 编写的原始 Tor 即将被淘汰，而用 Rust 编写的新 Tor 实现正在进行中，但尚未达到所需的程度。请耐心等待。
  - question: 为什么Orbot在iOS上频繁重新连接？
    answer: 参阅上面的答案——由于您所看到的 Tor 网络的切片大小，您可能会达到 50 MB 的限制。在这种情况下，iOS 会杀死“网络扩展”。如果您选择了“出现错误时重新启动”（默认为启用），它将自动尝试重新启动。
  - question: 如何调整 Orbot iOS 以使其正常工作？
    answer: 尝试清除缓存。有时删除旧信息可能会释放足够的内存。然而，重新发现每个节点实际上比从缓存中加载它占用 *更多* 内存。因此，如果它第一次没有启动，请给它更多重新启动的机会。它将从缓存中加载越来越多的当前信息，从而为正常操作留下更多内存。
  - question: 使用新缓存后启动正常，但过一会儿又会崩溃。
    answer: 转到“设置”，启用“启动前始终清除缓存”。这将需要更长的时间，但只要看到的网络片段不是很大，就可以重新启动。
  - answer: 我们将其设置得较低（默认为 5 MB），这样就不会太快达到 50 MB 的上限。您可以尝试将其设置得更高。转到设置，在“高级 Tor 配置”部分的一行输入
      `--MaxMemInQueues`（两个减号！），在下一行输入 `10 MB`。重启。如果您最终陷入重启循环，则说明您使用了太多内存。在这种情况下，请再次删除这些行。
    question: Tor 在日志中抱怨“MaxMemInQueues”太低，无法构建线路！
  - question: 没什么用，Tor 无法启动。
    answer: 尝试使用自定义网桥，即使您不需要它们来规避屏蔽。通过网桥看到的 Tor 网络片段可能较小，因此 Tor 客户端不会消耗太多内存。
  - question: 网桥似乎被屏蔽了！
    answer: 点击“询问 Tor”——将会更新内置的 obfs4 网桥列表，更新 Snowflake 配置，为您提供大量自定义网桥。再次尝试所有组合。您也可以使用
      Telegram 或电子邮件机器人，将会提供与其他存储库不同的网桥。
  - answer: Apple 推出了一种名为 `WKWebView` 的全新（更快）Web 渲染器，取代了 `UIWebView`，并希望所有应用都转用该渲染器。然而，`WKWebView`
      并不像 `UIWebView` 那样支持代理流量。此外，由于我们无法代理音频/视频流，也无法通过 WebRTC 泄露您的 IP 地址，因此代理功能始终只是一种辅助工具。有了
      Orbot，这些问题都不复存在。不幸的是，Orbot iOS 现在意外地陷入了这个很难摆脱的困境。不过，自 iOS 17 起，`WKWebView` 支持代理，所以您现在又有了选择。如果可以，请更新到
      iOS 17！
    question: Onion Browser 新版本为何依赖于 Orbot iOS？
home:
  title: 'Orbot - 让应用更安全。'
  description: 'Orbot 是适用于 Android 的 Tor VPN。它能让应用更安全！'
  locale: '中文（简体）'
  banner_area:
    subtitle: 'Orbot - 适用于智能手机的 Tor VPN'
    title: '让应用更安全'
    app_store_url: 'https://play.google.com/store/apps/details?id=org.torproject.android'
    app_store_button: '/assets/img/en-play-badge.png'
    apple_url: 'https://apps.apple.com/us/app/orbot/id1609461599'
    apple_button: '/assets/img/apple.png'
    fdroid_url: 'https://guardianproject.info/fdroid'
    fdroid_button: '/assets/img/fdroid.png'
    image: '/assets/img/LandingPage.png'
  promo_area:
    promo_area_items:
    - title: '流量隐私'
      text: '来自任何应用的加密流量通过 Tor 网络，为您提供最高标准的安全和隐私保护。'
      image: '/assets/img/Browsing.png'
      alt: 'Browsing-icon'
    - title: '阻止监视'
      text: '没有任何监视者知道您在什么时候使用什么应用，也无法阻止您使用它们。'
      image: '/assets/img/NoExtras.png'
      alt: 'NoExtras-icon'
    - title: '安全无痕'
      text: '您的网络运营商和应用服务器不会集中记录您的流量历史或 IP 地址。'
      image: '/assets/img/TossHistory.png'
      alt: 'TossHistory-icon'
  power_area:
    title: '由 Tor 提供支持'
    text: 'Orbot 是您在 Android 和 iOS 上使用 Tor 的可靠连接。'
    tor_image: '/assets/img/Tor.png'
    link_text: '了解更多 >'
    link_url: 'about'
  detail_area:
    detail_area_items:
    - title: '保护并代理<br/>特定应用。'
      text: 'Orbot（在 Android 上）允许您选择要通过 Tor 路由的应用，您仍然可以使用可能来自 Tor 流量的应用和服务。'
      image: '/assets/img/AppChoice.jpg'
      alt: 'AppChoice-image'
    - title: '超快速、超安全地访问常用的洋葱服务。'
      text: '<a href="https://support.torproject.org/onionservices/" target="_blank">洋葱服务</a>是只能通过
        Tor 访问的网站版本。Orbot 允许任何应用连接到洋葱服务，也允许手机托管洋葱服务！'
      image: '/assets/img/OnionSites.jpg'
      alt: 'OnionSites-mage'
    - title: '时刻保持连接'
      text: 'Orbot提供多种多样的Tor网桥，使你能在最严格的网络审查下保持连接'
      image: '/assets/img/Bridges.jpg'
      alt: 'Bridges-image'
  featured_area:
    title: '特别赞助商'
    featured_area_items:
    - image: '/assets/img/NewYorkTimes.png'
      alt: 'NewYorkTimes-logo'
      url: "http://www.nytimes.com/2014/02/20/technology/personaltech/privacy-please-tools-to-shield-your-smartphone-from-snoopers.html"
    - image: '/assets/img/ArsTechnica.jpg'
      alt: 'ArsTechnica-logo'
      url: "https://arstechnica.com/information-technology/2012/02/from-encryption-to-darknets-as-governments-snoop-activists-fight-back/"
    - image: '/assets/img/TechCrunch.png'
      alt: 'TechCrunch-logo'
      url: "https://techcrunch.com/2016/01/20/facebook-expands-tor-support-to-android-orbot-proxy/"
    - image: '/assets/img/BoingBoing.png'
      alt: 'BoingBoing-logo'
      url: "https://boingboing.net/2018/10/23/hardware-orbot.html"
    - image: '/assets/img/LifeHacker.png'
      alt: 'LifeHacker-logo'
      url: "https://lifehacker.com/the-apps-that-protect-you-against-verizons-mobile-track-1679936224"
    - image: '/assets/img/Gizmodo.png'
      alt: 'Gizmodo-logo'
      url: "https://gizmodo.com/6-apps-to-secure-your-smartphone-better-1791777911"
  special_area:
    title: '“Orbot 是与我需要的应用程序保持连接的最安全方式！”'
    text: '来自Play Store的评论-2018.05.21'
  dev_area:
    title: '关于开发者。'
    text: 'Orbot 是<a href="https://github.com/guardianproject/orbot/">自由开源</a>的。任何人都可以帮助贡献、审查或检查代码。主要的贡献程序员包括<a
      href="https://github.com/n8fr8">Nathan Freitas</a>、<a href="https://gitlab.com/eighthave">Hans-Christoph
      Steiner</a>、<a href="https://github.com/bitmold">Bim</a>、<a href="https://die.netzarchitekten.com/">Benjamin
      Erhart</a>等，来自<a href="https://guardianproject.info/">Guardian Project</a>的更多人员。'
    github_url: 'https://github.com/guardianproject/orbot'
    github_label: '在GitHub上关注@GuardianProject'
    github_text: '关注@GuardianProject'
legal:
  title: 'Orbot | 合法性'
  description: 'Orbot - 合法性界面。'
  title_section: '合法的'
  legal_items:
  - text: |
      **Orbot** 版权 © 2009-2022 Nathan Freitas 和 Guardian Project。保留所有权利。**"Orbot"** 和 Orbot 标志是 Nathan Freitas 的商标。Orbot 根据 [3-clause BSD 许可证](https://opensource.org/licenses/BSD-3-Clause) 分发。
  - text: '该软件使用强加密技术，可能受到世界其他地区的出口/进口和/或使用限制。在使用任何加密软件之前，请查看您国家关于加密软件的进口、拥有或使用以及再出口的法律、法规和政策，以确定是否被允许。更多信息请参阅
      [https://www.wassenaar.org](https://www.wassenaar.org)。'
  - text: '如果您是开发者，并计划以源代码或二进制形式重新分发这个应用程序，请阅读 [LICENSE 文件](https://github.com/guardianproject/orbot/blob/master/LICENSE)
      以获取完整的许可详情和法律义务。'
navigation:
  navigation_links:
  - title: '关于Orbot'
    url: '/about'
  - title: '隐私政策'
    url: '/privacy-policy'
  - title: '下载'
    url: '/download'
  - title: '源代码'
    url: '/code'
  - title: '支持'
    url: '/faqs'
  - title: '法律政策'
    url: '/法律政策'
  - title: '捐赠'
    url: '/捐赠'
privacy:
  title: 'Orbot | 隐私政策'
  description: 'Orbot - 隐私政策页面。'
  title_section: '隐私政策'
  privacy_items:
  - text: "Orbot 不直接收集用户活动数据。除了内置并由 Google Play 跟踪的信息外，Orbot 不使用任何第三方分析工具。"
  - text: "你可以通过阅读[Tor FAQ](https://2019.www.torproject.org/docs/faq.html.en)来更深入了解
      Tor 如何保护你的隐私"
  - text: "你可以通过查看 [数据使用和保护政策](https://guardianproject.info/2016/05/04/data-usage-and-protection-policies/)
      页面，更深入了解 Guardian Project 对隐私的处理方法。"
dir: ltr
