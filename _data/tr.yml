about:
  special_area:
    title: 'Orbot - Akıllı Telefonlar için Tor VPN'
    subtitle: 'Uygulama trafiğinizi gizli ve engelsiz tutun.'
  tor_area:
    image: '/assets/img/Tor.png'
    description: 'Tor, çeşitli insanlardan oluşan bir ağ tarafından desteklenmektedir.
      Tor kullandığınızda hiçbir şirket veya kişi gezinme etkinliğinize erişemez.'
    text: 'Tor hakkında bilgi edinmek için'
    url: 'https://torproject.org/tr/'
    tor_items:
    - image: '/assets/img/EncryptedTraffic.png'
      alt: 'EncryptedTraffic-icon'
      title: 'Şifreli trafik.'
      text: 'İnternet trafiğiniz şifrelenir ve hedefine ulaşmadan önce dünyanın üç
        farklı yerinden geçirilir.'
    - image: '/assets/img/OnlinePrivacy.png'
      alt: 'Çevrim içi gizlilik simgesi'
      title: 'Çevrim içi gizlilik.'
      text: 'Üçüncü taraflar hangi uygulamaları kullandığınızı bilmeyecek. Gerçek
        IP adresiniz açığa çıkmaz.'
    - image: '/assets/img/NoCensorship.png'
      alt: 'Sansür yok simgesi'
      title: 'Sansür yok.'
      text: 'Coğrafyanız veya ağ kısıtlamalarınız ne olursa olsun, uygulamalara ve
        içeriğe erişin.'
    - image: '/assets/img/NoSurveillance.png'
      alt: 'Gözetim yok simgesi'
      title: 'Gözetim yok.'
      text: 'İnternet hizmeti sağlayıcıları (ISP) ve kablosuz ağ işleticileri gezindiğiniz
        yerleri göremez.'
    - image: '/assets/img/NoTracking.png'
      alt: 'İzlenme yok simgesi'
      title: 'İzlenme yok.'
      text: 'Denetim sizde. Üçüncü tarafların sizi izlemesini devre dışı bırakın.'
  connected_area:
    image: '/assets/img/AboutScreens.png'
    link: 'https://www.csoonline.com/article/3287653/what-is-the-tor-browser-how-it-works-and-how-it-can-help-you-protect-your-identity-online.html'
  disclaimer_area:
    disclaimer_items:
    - title: 'Sorumluluk reddi'
      text_nodes:
      - node: 'Orbot kullanmak tek başına güvenlik veya gizliliği garanti etmez. Ağ
          trafiğinizin gizliliğini ve anonimliğini artırabilecek bir dizi özellik
          sağlar. Lütfen şunları unutmayın…'
      - node: 'Normalde Tor ağı dışından eriştiğiniz uygulamalarda oturum açmak için
          Orbot kullanırsanız, uygulama sizi tanıyabilir ve Tor kullandığınızı bilebilir.
          Belirli durumlarda (örneğin, baskıcı ülkelerdeki siyasi muhalefet), bu kendi
          başına suçlayıcı bir bilgi olabilir.'
    - title: 'Coğrafi Konum, Medya ve Mobil Bilgi'
      text_nodes:
      - node: 'Uygulamalar Coğrafi Konum API''sini kullanabilir (geçerli GPS konumunuzu
          görüntülemek için), harici depolama alanındaki fotoğraf ve videolara erişebilir
          veya cep telefonunuz hakkında telefon numarası ve operatör bilgileri gibi
          verileri alabilir. Kullanıcılar ayrıca erişim için izin isteyen açılır pencerelere
          karşı da dikkatli olmalıdır.'
    - title: 'Daha Fazla Bilgi'
      text_nodes:
      - node: 'Tor Projesi, Tor kullanırken anonim kalmakla ilgili <a href="https://support.torproject.org/faq/staying-anonymous/">küçük
          bir ipucu sayfası</a> tutmaktadır.'
      - node: 'Önceki güvenlik sorunları, hata düzeltmeleri ve daha fazlası dahil
          olmak üzere Orbot <a href="https://github.com/guardianproject/orbot/releases">sürüm
          notlarının</a> listesi <a href="https://github.com/guardianproject/orbot/releases">burada
          bulunabilir</a>. <a href="https://github.com/guardianproject/orbot/issues">Bilinen
          sorunların güncel listesi</a> için GitHub projesine de bakabilirsiniz.'
  title: Orbot - Uygulamaları Güvende Tutun
  description: Orbot, Android için bir Tor VPN'dir. Uygulamaları güvende tutar!
code:
  title: 'Orbot | Kod'
  code_items:
  - question: 'Android için Orbot'
    answer: 'Kaynak kodu [GitHub](https://github.com/guardianproject/orbot/) ve [Gitlab](https://gitlab.com/guardianproject/orbot/)
      adreslerinde bulunabilir'
  - question: 'iOS için Orbot'
    answer: 'Kaynak kodu [GitHub](https://github.com/guardianproject/orbot-ios) adresinde
      bulunabilir'
  - question: 'Tor-Android'
    answer: 'Orbot, herhangi bir uygulamanın Tor''u içinde paketlemesine ve yönetmesine
      olanak tanıyan çekirdek [Tor-Android](https://gitlab.com/guardianproject/tor-android/)
      kütüphanesi üzerine kuruludur'
  - question: 'iOS için Tor.framework'
    answer: 'iOS için Orbot, herhangi bir uygulamanın bütünleşebildiği CocoaPad olarak
      kullanılabilen çekirdek [Tor.framework](https://github.com/iCepa/Tor.framework/tree/pure_pod)
      projesi üzerine kuruludur'
  - question: 'IPtProxy'
    answer: 'Orbot ayrıca [IPtProxy Kütüphanesi](https://github.com/tladesignz/IPtProxy)
      kullanarak Obfs4 ve [Snowflake](https://snowflake.torproject.org/) gibi Tor
      Eklenebilir Aktarımları için destek içerir'
  title_section: Kod
  description: Orbot Açık Kaynak Kodu
donate:
  title: 'Orbot''u Destekleyin'
  description: 'Orbot, neredeyse tamamen kuruluşlardan ve bireylerden gelen hibeler
    ve bağışlarla finanse edilen bir kuruluş olan Guardian Project tarafından geliştirilmektedir.'
  subtitle: 'Bağış yapma yolları'
  donate_form:
  - text: '10 dolar gönderin'
    value: '10'
  - text: '20 dolar gönderin'
    value: '20'
  - text: '30 dolar gönderin'
    value: '30'
  - text: '40 dolar gönderin'
    value: '40'
  - text: '50 dolar gönderin'
    value: '50'
  donate_partners:
  - name: 'PayPal'
    url: 'https://paypal.me/guardianproject'
    image: '/assets/img/pp.jpg'
    alt: 'PayPal logosu'
    text: 'Tek seferlik bağış gönderin'
  - name: 'BitCoin'
    url: '198fHnKSkXboZLWqVi2KTRdNrPbdh34Ya5'
    image: '/assets/img/Bitcoin.png'
    alt: 'Bitcoin logosu'
  - name: 'Ether'
    url: '133170647542866872072615948397500080356915441689'
    image: '/assets/img/Ether.png'
    alt: 'Ether-logo'
  - name: 'Patreon'
    url: 'https://patreon.com/GuardianProject'
    image: '/assets/img/Patreon.png'
    alt: 'Patreon logosu'
    text: 'Bizi destekleyin'
  - name: 'Liberapay'
    url: 'https://liberapay.com/GuardianProject'
    image: '/assets/img/liberapay.svg'
    alt: 'Liberapay-img'
    text: 'Bize bağış yapın'
  - name: 'GitHub Sponsorları'
    url: 'https://github.com/sponsors/eighthave'
    image: '/assets/img/github.png'
    alt: 'github logosu'
    text: 'Bize sponsor olun'
download:
  special_area:
    title: 'Orbot - Akıllı Telefonlar için Tor VPN'
    subtitle: 'Orbot İndir!'
  tor_area:
    text: 'Diğer Tor Projesi yazılımlarını indirin'
    url: 'https://www.torproject.org/download/'
    tor_items:
    - title: 'Google Play'
      info: 'Uygulamayı Google Play [https://play.google.com/store/apps/details?id=org.torproject.android](https://play.google.com/store/apps/details?id=org.torproject.android)
        adresinden kurabilirsiniz'
    - title: 'F-Droid'
      info: 'Guardian Project F-Droid uygulama deposunu nasıl ekleyeceğinizi [https://guardianproject.info/fdroid](https://guardianproject.info/fdroid)
        adresinden öğrenebilirsiniz'
    - title: 'Apple App Store'
      info: 'Resmi iOS için Orbot sürümünü [https://apps.apple.com/us/app/orbot/id1609461599](https://apps.apple.com/us/app/orbot/id1609461599)
        adresinden kurabilirsiniz. Ayrıca [https://testflight.apple.com/join/adSqbCeM](https://testflight.apple.com/join/adSqbCeM)
        üzerinden Testflight erken geri bildirim grubumuza katılabilirsiniz'
    - title: 'Doğrudan İndir'
      info: 'Test için indirilebilir dosyalarla etiketlenmiş sürümleri GitHub''da
        [https://github.com/guardianproject/orbot/releases](https://github.com/guardianproject/orbot/releases)
        ve [https://github.com/guardianproject/orbot-ios/releases](https://github.com/guardianproject/orbot-ios/releases)
        adreslerinde bulabilirsiniz'
    - title: 'Guardian Project Web Sitesi'
      info: 'Orbot Android uygulama dosyasını (APK) doğrudan [https://guardianproject.info/releases/orbot-latest.apk](https://guardianproject.info/releases/orbot-latest.apk)
        adresinden indirebilirsiniz'
faqs:
  title: 'Orbot - SSS'
  faq_items:
  - question: 'En son sürüm, düzeltmeler ve iyileştirmelerle ilgili bilgileri nerede
      bulabilirim?'
    answer: 'Tüm sürümleri ve sürüm notlarını [https://github.com/guardianproject/orbot/releases](https://github.com/guardianproject/orbot/releases)
      ve [https://github.com/guardianproject/orbot-ios/releases](https://github.com/guardianproject/orbot-ios/releases)
      adreslerindeki açık kaynaklı proje sayfalarımızda yayınlıyoruz'
  - question: 'Tor nedir?'
    answer: 'Tor, dünyanın dört bir yanındaki gönüllüler tarafından işletilen ve iletişiminizi
      onlar üzerinden geçirmenize izin veren bir aktarıcı ağıdır, bu da ziyaret ettiğiniz
      web sitelerini internet bağlantınızı izleyen kişilerden gizlemenize olanak tanır,
      ayrıca ziyaret ettiğiniz web sitelerinin fiziksel konumunuzu öğrenmesini de
      önler.'
  - question: 'Tor''a bağlıyken internet neden daha yavaş?'
    answer: 'Çünkü trafiğinizi dünyanın dört bir yanındaki gönüllüler tarafından işletilen
      aktarıcılar üzerinden geçirdiğiniz için bağlantınız darboğazlardan ve ağ gecikmesinden
      etkilenecektir.'
  - question: 'Tor''a bağlı olduğumu nasıl bilebilirim?'
    answer: 'Orbot''u açtığınızda, büyük düğmeye basın ve Tor ile bağlantı kurduktan
      birkaç saniye sonra %100 bağlı olduğunu belirten bir mesaj göreceksiniz ve düğme
      yeşile dönecektir. VPN''i bir tarayıcının trafiğini Tor üzerinden yönlendirmek
      için kullanıyorsanız, Tor''a bağlı olup olmadığınızı size söylemek için Tor
      ekibi tarafından oluşturulan bir bağlantı olan [https://check.torproject.org/](https://check.torproject.org/)
      adresine giderek bağlantınızı denetleyebilirsiniz.'
  - question: 'Köprüler nedir?'
    answer: 'Köprüler, sansürü aşmaya yardımcı olan Tor aktarıcılarıdır. Tor internet
      hizmeti sağlayıcınız tarafından engelleniyorsa köprüleri deneyebilirsiniz.'
  - question: iOS için Orbot neden bu kadar güvenilmez?
    answer: Tor ağına yönelik saldırılar Ukrayna savaşından bu yana arttı. O zamandan
      bu yana güvenlik açıkları düzeltildi ve düğümler eklendi ancak yine daha fazla
      bellek kullanıyor. Tor istemcisi ağdaki değişikliklere duyarlıdır çünkü tümünü
      keşfetmek ister. Ne kadar çok düğüm bulursa, o kadar çok bellek kullanır. Ne
      yazık ki Apple, "Ağ eklentileri" ("VPN" tarzı uygulamalar için kullanılması
      gereken API) olarak adlandırılan aygıtlarda yalnızca 50 MB RAM (mega bayt, en
      az 3 GB - GIGA bayt olan aygıtlarda!) kullanılmasına izin veriyor). Tor gibi
      bir yazılımın bu sınıra uyması çok zor. Ek olarak, şu anda kullanılan C ile
      yazılmış özgün Tor kullanımdan kaldırılırken, Rust ile yazılmış yeni bir Tor
      uygulaması da yolda, ancak henüz olması gereken yerde değil. Lütfen sabırlı
      olun.
  - answer: Yukarıdaki yanıta bakın. Gördüğünüz Tor ağı diliminin boyutundan dolayı
      50 MB sınırına ulaşabilirsiniz. iOS bu durumda "Ağ eklentisini" öldürür. "Hata
      durumunda yeniden başlat" seçeneğini (varsayılan olarak açık) seçtiyseniz, uygulamayı
      otomatik olarak yeniden başlatmayı dener.
    question: iOS için Orbot neden sürekli yeniden bağlanıyor?
  - answer: Ön belleği silmeyi deneyin. Bazen eski bilgilerden kurtulmak bellekte
      yeterli miktarda yer açabilir. Ancak her düğümü yeniden keşfetmek aslında onu
      ön bellekten yüklemekten *daha fazla* bellek kullanır. Bu nedenle, ilk seferinde
      başlamazsa, yeniden başlaması için biraz daha şans verin. Normal işlemler için
      ön bellekten daha fazla güncel bilgi yüklemeye başlayacak ve daha fazla belleği
      serbest bırakacaktır.
    question: iOS için Orbot uygulamasının çalışmasını sağlamak için ne yapabilirim?
  - question: Boş ön bellekle iyi başlıyor, ancak bir süre sonra yeniden çöküyor.
    answer: Ayarlar bölümüne gidin, "Başlatmadan önce her zaman ön belleği temizle"
      seçeneğini etkinleştirin. Daha uzun sürecek, ancak yeni bir başlangıçta gördüğünüz
      ağın dilimi çok büyük olmadığı sürece yeniden başlayacaktır.
  - answer: Bunu düşük bir değere ayarladık (varsayılan olarak 5 MB), böylece 50 MB
      tavanına çok hızlı ulaşmayacağız. Bunu daha yüksek bir değere ayarlamayı deneyebilirsiniz.
      Ayarlar bölümüne gidin. "Gelişmiş Tor yapılandırması" bölümünde `--MaxMemInQueuees`
      (iki tire ile!) yazın, sonraki satıra `10 MB` yazın. Yeniden başlatın. Yeniden
      başlatma döngüsüne girerseniz çok fazla bellek kullanıyorsunuz demektir. Bu
      durumda bu satırları kaldırın.
    question: Tor, günlükte "MaxMemInQueues" değerinin çok düşük olduğundan ve devreler
      kuramadığından şikayet ediyor!
  - question: Hiçbir şey işe yaramıyor, Tor başlamıyor.
    answer: Engellemeyi aşmak için gerek duymuyor olsanız bile özel köprüler kullanmayı
      deneyin. Köprüler arasından gördüğünüz Tor ağı dilimi daha küçük olabilir, böylece
      Tor istemcisi çok fazla bellek kullanmaz.
  - question: Köprüler engellenmiş gibi görünüyor!
    answer: "\"Tor'a sor\" üzerine dokunun. İç Obfs4 köprü listesi güncellenir, Snowflake
      yapılandırması güncellenir ve size bazı özel köprüler sağlar. Tüm kombinasyonları
      yeniden deneyin. Ayrıca Telegram ya da e-posta botunu da kullanabilirsiniz.
      Bunlar diğer paketlerden farklı köprüler sağlar."
  - answer: Apple, `UIWebView` yerine geçen `WKWebView` adlı yeni (daha hızlı) bir
      web oluşturucuyu tanıttı ve tüm uygulamaların buna taşınmasını istedi. Ancak
      `WKWebView`, trafiği `UIWebView` gibi bir vekil sunucu olarak desteklemiyor.
      Üstelik bu her zaman yalnızca bir destekti, çünkü ses/video akışlarını hiçbir
      zaman vekil sunucu olarak aktaramadık ve WebRTC üzerinden IP adresinizi sızdırdık.
      Orbot ile bu sorunlar ortadan kalktı. Ne yazık ki iOS için Orbot artık kazara,
      içinden çıkmanın zor olduğu bu köşeye düştü. Ancak iOS 17 sürümünden bu yana
      `WKWebView` vekil sunucu oluşturmayı desteklediğinden artık yeniden bir seçeneğiniz
      var. Yapabiliyorsanız lütfen iOS 17 sürümüne güncelleyin!
    question: Neden yeni Onion Browser sürümü iOS için Orbot uygulamasını kullanıyor?
  description: Orbot SSS sayfası
  title_section: SSS
home:
  locale: 'Türkçe'
  banner_area:
    subtitle: 'Orbot - Akıllı Telefonlar için Tor VPN'
    title: 'Uygulamaları Güvende Tutun'
    app_store_url: 'https://play.google.com/store/apps/details?id=org.torproject.android'
    app_store_button: '/assets/img/en-play-badge.png'
    apple_url: 'https://apps.apple.com/us/app/orbot/id1609461599'
    apple_button: '/assets/img/apple.png'
    fdroid_url: 'https://guardianproject.info/fdroid'
    fdroid_button: '/assets/img/fdroid.png'
    image: '/assets/img/LandingPage.png'
  promo_area:
    promo_area_items:
    - title: 'Trafik Gizliliği'
      text: 'Tor ağı aracılığıyla herhangi bir uygulamadan gelen şifreli trafik, size
        en yüksek güvenlik ve gizlilik standardını sunar.'
      image: '/assets/img/Browsing.png'
      alt: 'Gezinme simgesi'
    - title: 'Gözetlemeyi Durdurun'
      text: 'Hiçbir fazladan göz hangi uygulamaları ne zaman kullandığınızı bilemez
        veya bunları kullanmanızı engelleyemez.'
      image: '/assets/img/NoExtras.png'
      alt: 'Fazla yok simgesi'
    - title: 'Korunan Geçmiş'
      text: 'Ağ işleticiniz ve uygulama sunucularınız tarafından trafik geçmişiniz
        veya IP adresiniz merkezi olarak kaydedilmez.'
      image: '/assets/img/TossHistory.png'
      alt: 'Geçmişi at simgesi'
  power_area:
    title: 'Tor tarafından desteklenmektedir'
    text: 'Orbot, Android ve iOS''ta Tor için güvenilir bağlantınızdır.'
    tor_image: '/assets/img/Tor.png'
    link_text: 'Daha fazla bilgi edinin >'
    link_url: 'about'
  detail_area:
    detail_area_items:
    - title: 'Belirli uygulamaları koruyun<br/>ve engelini kaldırın.'
      text: 'Orbot (Android''de), hangi uygulamaların özellikle Tor üzerinden yönlendirileceğini
        seçmenize olanak tanıyarak Tor''dan gelen trafikle sorun oluşturabilecek uygulamaları
        ve hizmetleri kullanmaya devam etmenizi sağlar.'
      image: '/assets/img/AppChoice.jpg'
      alt: 'Uygulama seçimi resmi'
    - title: 'Popüler onion hizmetlerine çok hızlı, çok güvenli erişim.'
      text: '<a href="https://support.torproject.org/onionservices/" target="_blank">Onion
        hizmetleri</a> sitelerin yalnızca Tor ile erişilebilen sürümleridir. Orbot,
        herhangi bir uygulamanın bir onion hizmetine bağlanmasına ve telefonunuzun
        bir onion hizmetini barındırmasına olanak tanır!'
      image: '/assets/img/OnionSites.jpg'
      alt: 'Onion siteleri'
    - title: 'Bağlı Kalın'
      text: 'Orbot, çeşitli Tor köprülerine erişim sunar, böylece en kısıtlı ağlarda
        bile bağlantıda kalabilirsiniz'
      image: '/assets/img/Bridges.jpg'
      alt: 'Köprüler resmi'
  featured_area:
    title: 'Öne çıkanlar'
    featured_area_items:
    - image: '/assets/img/NewYorkTimes.png'
      alt: 'NewYorkTimes-logo'
      url: "http://www.nytimes.com/2014/02/20/technology/personaltech/privacy-please-tools-to-shield-your-smartphone-from-snoopers.html"
    - image: '/assets/img/ArsTechnica.jpg'
      alt: 'ArsTechnica-logo'
      url: "https://arstechnica.com/information-technology/2012/02/from-encryption-to-darknets-as-governments-snoop-activists-fight-back/"
    - image: '/assets/img/TechCrunch.png'
      alt: 'TechCrunch-logo'
      url: "https://techcrunch.com/2016/01/20/facebook-expands-tor-support-to-android-orbot-proxy/"
    - image: '/assets/img/BoingBoing.png'
      alt: 'BoingBoing-logo'
      url: "https://boingboing.net/2018/10/23/hardware-orbot.html"
    - image: '/assets/img/LifeHacker.png'
      alt: 'LifeHacker-logo'
      url: "https://lifehacker.com/the-apps-that-protect-you-against-verizons-mobile-track-1679936224"
    - image: '/assets/img/Gizmodo.png'
      alt: 'Gizmodo-logo'
      url: "https://gizmodo.com/6-apps-to-secure-your-smartphone-better-1791777911"
  special_area:
    title: '"Orbot, ihtiyacım olan uygulamalara bağlı kalmanın en güvenli yolu!"'
    text: 'Play Store yorumu - 21 Mayıs 2018'
  dev_area:
    title: 'Geliştiriciler hakkında.'
    text: 'Orbot <a href="https://github.com/guardianproject/orbot/">özgür ve açık
      kaynak kodludur</a>. Herkes katkıda bulunabilir, kodu denetleyebilir veya inceleyebilir.
      Katkıda bulunan başlıca programcılar arasında <a href="https://github.com/n8fr8">Nathan
      Freitas</a>, <a href="https://gitlab.com/eighthave">Hans-Christoph Steiner</a>,
      <a href="https://github.com/bitmold">Bim</a>, <a href="https://die.netzarchitekten.com/">Benjamin
      Erhart</a> ve <a href="https://guardianproject.info/">Guardian Project</a>''den
      daha birçok kişi bulunmaktadır.'
    github_url: 'https://github.com/guardianproject/orbot'
    github_label: 'GitHub''da takip edin: @GuardianProject'
    github_text: 'Takip edin: @GuardianProject'
  description: Orbot, Android için bir Tor VPN'dir. Uygulamaları güvende tutar!
  title: Orbot - Uygulamayı Güvenli Tutun.
legal:
  title: 'Orbot | Yasal'
  legal_items:
  - text: |
      **Orbot** telif hakkı © 2009-2022 Nathan Freitas ve Guardian Project'e aittir. Tüm hakları saklıdır. **"Orbot "** ve Orbot logosu Nathan Freitas'ın ticari markalarıdır. Orbot [3 maddelik BSD lisansı](https://opensource.org/licenses/BSD-3-Clause) koşulları altında dağıtılmaktadır.
  - text: 'Bu yazılım güçlü kriptografi kullanır ve dünyanın diğer bazı bölgelerinde
      belirli ihracat/ithalat ve/veya kullanım kısıtlamalarına tabi olabilir. Herhangi
      bir şifreleme yazılımını kullanmadan ÖNCE, buna izin verilip verilmediğini görmek
      için lütfen ülkenizin şifreleme yazılımının ithalatı, bulundurulması veya kullanımı
      ve yeniden ihracatı ile ilgili yasalarına, yönetmeliklerine ve politikalarına
      bakın. Daha fazla bilgi için [https://www.wassenaar.org](https://www.wassenaar.org)
      adresine bakın.'
  - text: 'Bir geliştiriciyseniz ve bu uygulamayı kaynak kodu veya derlenmiş biçimde
      yeniden dağıtmayı planlıyorsanız, tüm lisans ayrıntıları ve yasal yükümlülükler
      için lütfen [LICENSE dosyasını](https://github.com/guardianproject/orbot/blob/master/LICENSE)
      okuyun.'
  description: Orbot - Yasal konular sayfası.
  title_section: Yasal
navigation:
  navigation_links:
  - title: 'Hakkında'
    url: '/about'
  - title: 'Gizlilik'
    url: '/privacy-policy'
  - title: 'İndir'
    url: '/download'
  - title: 'Kod'
    url: '/code'
  - title: 'SSS'
    url: '/faqs'
  - title: 'Yasal'
    url: '/legal'
  - title: 'Bağış yap'
    url: '/donate'
privacy:
  title: 'Orbot | Gizlilik Politikası'
  privacy_items:
  - text: "Orbot, kullanıcı etkinliği hakkında doğrudan herhangi bir veri toplamaz.
      Google Play'de yerleşik olan ve Google Play tarafından izlenenler dışında, Orbot
      herhangi bir üçüncü taraf istatistiği kullanmaz."
  - text: "Tor'un gizliliğinizi nasıl koruduğu hakkında daha fazla bilgi edinmek için
      [Tor SSS](https://2019.www.torproject.org/docs/faq.html.en) sayfasını okuyabilirsiniz"
  - text: "Guardian Project'in gizlilik yaklaşımı hakkında daha fazla bilgi edinmek
      için [Veri Kullanımı ve Koruma Politikaları](https://guardianproject.info/2016/05/04/data-usage-and-protection-policies/)
      sayfasını ziyaret edebilirsiniz."
  title_section: Gizlilik Politikası
  description: Orbot - Gizlilik politikası sayfası.
bottom_contact_area:
  title: İletişime geçin.
  href: https://guardianproject.info/contact
  action: İletişim
  content: Projeyi başka bir şekilde parasal olarak desteklemek ister misiniz? Lütfen
    iletişime geçin.
bottom_donate_area:
  title: Özgür ve açık.
  content: Orbot özgür bir uygulamadır! Uygulamanın sürekli gelişimini destekleyin.
    Sevgiyi paylaşın.
  action: Bağış yap
main_navigation:
  donate: Bağış yap
  responsive_menu_links:
  - url: /
    title: Ana sayfa
  - url: /about/
    title: Hakkında
  - url: /privacy-policy/
    title: Gizlilik
  - title: Yasal
    url: /legal/
  - url: /download/
    title: İndir
  - url: /faqs/
    title: SSS
  - url: /donate/
    title: Bağış yap
locale_selector:
  title: Dil
site:
  title: Orbot - Mobil için Tor
footer:
  copyright: Telif Hakkı © 2009-%Y Guardian Project. Tüm hakları saklıdır.
dir: ltr
