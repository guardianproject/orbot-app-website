# frozen_string_literal: true

# Creates a page in root site from pages on main language and redirect
# to them
Jekyll::Hooks.register :site, :post_read do |site|
  next unless site.default_locale?

  redirector = site.pages.find do |p|
    p.name == 'redirector.html'
  end

  redirectors = []

  # Create a redirector page for 
  site.pages.each do |page|
    next if page == redirector
    next unless page.extname == '.html'

    redirectors << Jekyll::PageWithoutAFile.new(site, site.source, '/redirectors', page.name).tap do |r|
      r.data['permalink'] = "/redirectors#{page.url}"
      r.data['redirect_to'] = page.url
      r.data['layout'] = redirector.data['layout']
      r.content = redirector.content.dup
    end
  end

  site.pages.concat redirectors
end

# Moves pages to the site root
Jekyll::Hooks.register :site, :post_write do |site|
  next unless site.default_locale?

  site.pages.each do |page|
    next unless page.data['redirect_to']

    dest = File.join(site.dest, '..', page.dir.sub('/redirectors', ''))

    FileUtils.mkdir_p(dest)
    FileUtils.mv page.destination(site.dest), dest
  end
end
